#include "particle_system.h"
#include "loaders/image/load_image.h"

const int MAX_PARTICLES_COUNT = 1000;

const int PARTICLE_TYPE_GENERATOR = 0;

void ParticleSystem::initialize() {
    glGenVertexArrays(2, vao);
    glGenBuffers(2, vbo);

    for (int i = 0; i < 2; i++) {
        int dataSize = 11 * sizeof(GLfloat) + sizeof(GLint);

        glBindVertexArray(vao[i]);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[i]);

        // Allocate the space for the maximum number of the particles
        glBufferData(GL_ARRAY_BUFFER, dataSize * MAX_PARTICLES_COUNT, nullptr, GL_DYNAMIC_DRAW);

        // Set the first particle type as the generator
        glBufferSubData(GL_ARRAY_BUFFER, 11 * sizeof(GLfloat), sizeof(GLint), &PARTICLE_TYPE_GENERATOR);

        // a_position
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, dataSize, (void*) 0);

        // a_velocity
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, dataSize, (void*) (3 * sizeof(GLfloat)));

        // a_color
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, dataSize, (void*) (6 * sizeof(GLfloat)));

        // a_lifetime
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, dataSize, (void*) (9 * sizeof(GLfloat)));

        // a_size
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, dataSize, (void*) (10 * sizeof(GLfloat)));

        // a_type
        glEnableVertexAttribArray(5);
        glVertexAttribIPointer(5, 1, GL_INT, dataSize, (void*) (11 * sizeof(GLfloat)));
    }

    Image image = loadImage("../res/image/particle.png", true);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.bytes.data());

    // Create the transform feedback
    glGenTransformFeedbacks(1, &transformFeedback);

    // Create the query for reading the particles count from OpenGL
    glGenQueries(1, &query);
}

void ParticleSystem::update() {
    // Discard the rendering
    glEnable(GL_RASTERIZER_DISCARD);

    glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transformFeedback);

    // Bind the current buffer for reading the data
    glBindVertexArray(vao[currentReadBuffer]);

    // Enable the a_velocity attribute because it is needed for updating the particles
    glEnableVertexAttribArray(1);

    // Bind the other buffer for writing the data
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vao[1 - currentReadBuffer]);

    // Begin the query
    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);

    glBeginTransformFeedback(GL_POINTS);

    // Update the particles using OpenGl
    glDrawArrays(GL_POINTS, 0, particlesCount);

    glEndTransformFeedback();

    // End the query and get the particles count
    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
    glGetQueryObjectiv(query, GL_QUERY_RESULT, &particlesCount);

    // Change the current buffer for reading the data
    currentReadBuffer = 1 - currentReadBuffer;

    glDisable(GL_RASTERIZER_DISCARD);
}

void ParticleSystem::draw() {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDepthMask(GL_FALSE);

    glBindVertexArray(vao[currentReadBuffer]);

    // Disable the a_velocity attribute because it isn't needed for rendering the particles
    glDisableVertexAttribArray(1);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glDrawArrays(GL_POINTS, 0, particlesCount);

    glDepthMask(GL_TRUE);

    glDisable(GL_BLEND);
}
