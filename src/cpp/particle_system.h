#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

/**
 * The particle system where the particles are updated using OpenGL
 */
class ParticleSystem {

public:

    /**
     * The vertex array objects
     */
    GLuint vao[2] { 0, 0 };

    /**
     * The vertex buffer objects
     */
    GLuint vbo[2] { 0, 0 };

    /**
     * The particle texture
     */
    GLuint texture = 0;

    /**
     * The transform feedback
     */
    GLuint transformFeedback = 0;

    /**
     * The query for reading the particles count from OpenGL
     */
    GLuint query = 0;

    /**
     * The current buffer for reading the data
     */
    int currentReadBuffer = 0;

    /**
     * The particles count
     */
    int particlesCount = 1;

    /**
     * The elapsed time since the last generation of new particles
     */
    float elapsedTime = 0;

    /**
     * The time when new particles are generated
     */
    float nextGenerationTime = 0.05;

    /**
     * The position where the particles are generated
     */
    glm::vec3 position = glm::vec3(0, 0, 0);

    /**
     * The gravity
     */
    glm::vec3 gravity = glm::vec3(0, -10, 0);

    /**
     * The constant particle velocity
     */
    glm::vec3 constantVelocity = glm::vec3(-1, 5, -1);

    /**
     * The variable particle velocity that is multiplied by the random number
     */
    glm::vec3 variableVelocity = glm::vec3(2, 5, 2);

    /**
     * The color of the generated particles
     */
    glm::vec3 color = glm::vec3(1, 1, 1);

    /**
     * The size of the generated particles
     */
    float size = 0.2;

    /**
     * The constant particle lifetime
     */
    float constantLifetime = 1.5;

    /**
     * The variable particle lifetime that is multiplied by the random number
     */
    float variableLifetime = 0.5;

    /**
     * The number of particles to genarate
     */
    int particlesToGenerate = 10;

public:

    void initialize();

    void update();

    void draw();

};
