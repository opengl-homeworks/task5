#version 400 core

uniform sampler2D u_texture; // The particle texture

in vec2 v_textureCoordinates; // The texture coordinates
in vec4 v_colorWithAlpha; // The particle color with the alpha based on the lifetime

out vec4 color; // The fragment color

void main() {
    // Set the fragment color based on the particle texture and color
    color = texture(u_texture, v_textureCoordinates) * v_colorWithAlpha;
}
