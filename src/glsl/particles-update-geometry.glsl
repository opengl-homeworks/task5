#version 400 core

const int PARTICLE_TYPE_GENERATOR = 0;
const int PARTICLE_TYPE_NORMAL = 1;

// The geometry shader input type
layout (points) in;

// The geometry shader output type and the maximum number of vertices that can be output
// This limit needs to be updated if there is a need to generate more particles (the maximum available value shoud be at least 256)
layout (points, max_vertices = 40) out;

uniform vec3 u_position; // The position where the particles are generated
uniform vec3 u_gravity; // The gravity
uniform vec3 u_constantVelocity; // The constant particle velocity
uniform vec3 u_variableVelocity; // The variable particle velocity that is multiplied by the random number
uniform vec3 u_color; // The color of the generated particles
uniform float u_size; // The size of the generated particles
uniform float u_constantLifetime; // The constant particle lifetime
uniform float u_variableLifetime; // The variable particle lifetime that is multiplied by the random number

uniform float u_deltaTime; // The elapsed time since the last frame in seconds

uniform vec3 u_randomSeed; // The seed for the random number function

uniform int u_particlesToGenerate; // The number of particles to genarate

// The variables that are passed from the vertex shader
in vec3 v_position[];
in vec3 v_velocity[];
in vec3 v_color[];
in float v_lifetime[];
in float v_size[];
in int v_type[];

// The variables that are output to the buffer
out vec3 position;
out vec3 velocity;
out vec3 color;
out float lifetime;
out float size;
out int type;

vec3 localSeed; // The local seed for the random number function that is updated

// Calculates the random number from zero to one
float rand() {
    uint n = floatBitsToUint(localSeed.y * 214013.0 + localSeed.x * 2531011.0 + localSeed.z * 141251.0);
    n = n * (n * n * 15731u + 789221u);
    n = (n >> 9u) | 0x3F800000u;

    float r =  2.0 - uintBitsToFloat(n);
    localSeed = vec3(localSeed.x + 147158.0 * r, localSeed.y * r  + 415161.0 * r, localSeed.z + 324154.0 * r);
    return r;
}

void main() {
    localSeed = u_randomSeed;

    // gl_Position is not set because the rendering is dicarded

    if (v_type[0] == PARTICLE_TYPE_GENERATOR) {
        // Copy the generator particle
        position = v_position[0];
        velocity = v_velocity[0];
        color = v_color[0];
        lifetime = v_lifetime[0];
        size = v_size[0];
        type = v_type[0];
        EmitVertex();
        EndPrimitive();

        // Genarate new particles
        for (int i = 0; i < u_particlesToGenerate; i++) {
            position = u_position;
            velocity = u_constantVelocity + u_variableVelocity * vec3(rand(), rand(), rand());
            color = u_color;
            lifetime = u_constantLifetime + u_variableLifetime * rand();
            size = u_size;
            type = PARTICLE_TYPE_NORMAL;
            EmitVertex();
            EndPrimitive();
        }
    }
    else if (v_lifetime[0] > 0.0) {
        // Update the particles that are alive
        position = v_position[0] + v_velocity[0] * u_deltaTime;
        velocity = v_velocity[0] + u_gravity * u_deltaTime;
        color = v_color[0];
        lifetime = v_lifetime[0] - u_deltaTime;
        size = v_size[0];
        type = v_type[0];
        EmitVertex();
        EndPrimitive();
    }
}
