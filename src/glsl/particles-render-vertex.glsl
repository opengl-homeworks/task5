#version 400 core

layout (location = 0) in vec3 a_position; // The particle position
layout (location = 2) in vec3 a_color; // The particle color
layout (location = 3) in float a_lifetime; // The particle lifetime
layout (location = 4) in float a_size; // The particle size
layout (location = 5) in int a_type; // The particle type

// The variables that are passed to the geometry shader
out vec3 v_color;
out float v_lifetime;
out float v_size;
out int v_type;

void main() {
    // Pass the position using the gl_Position variable
    gl_Position = vec4(a_position, 1.0);

    // Pass the variables to the geometry shader
    v_color = a_color;
    v_size = a_size;
    v_lifetime = a_lifetime;
    v_type = a_type;
}
