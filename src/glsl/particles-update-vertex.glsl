#version 400 core

layout (location = 0) in vec3 a_position; // The particle position
layout (location = 1) in vec3 a_velocity; // The particle velocity
layout (location = 2) in vec3 a_color; // The particle color
layout (location = 3) in float a_lifetime; // The particle lifetime
layout (location = 4) in float a_size; // The particle size
layout (location = 5) in int a_type; // The particle type

// The variables that are passed to the geometry shader
out vec3 v_position;
out vec3 v_velocity;
out vec3 v_color;
out float v_lifetime;
out float v_size;
out int v_type;

void main() {
    // Pass the variables to the geometry shader
    v_position = a_position;
    v_velocity = a_velocity;
    v_color = a_color;
    v_lifetime = a_lifetime;
    v_size = a_size;
    v_type = a_type;
}
