#version 400 core

struct AmbientLight {
    vec3 color;
};

struct DirectionalLight {
    vec3 color;
    vec3 direction;
};

struct PointLight {
    vec3 color;
    vec3 position;
    float attenuation;
    bool enabled;
};

struct SpotLight {
    vec3 color;
    vec3 position;
    vec3 direction;
    float innerAngleCos;
    float outerAngleCos;
    float attenuation;
    bool enabled;
};

struct Colors {
    vec3 light;
    vec3 specular;
};

vec3 calculateDirectionalLightColor(DirectionalLight directionalLight, vec3 normal);

Colors calculatePointLightColor(PointLight pointLight, vec3 cameraPosition, vec3 normal, vec3 position, float specular, float shininess);

Colors calculateSpotLightColor(SpotLight spotLight, vec3 cameraPosition, vec3 normal, vec3 position, float specular, float shininess);

uniform vec3 u_cameraPosition; // The camera position
uniform float u_specular; // The specular parameter (how much the specular lighting affects the object)
uniform float u_shininess; // The shininess parameter (how reflective the object is, opposite to roughness)
uniform AmbientLight u_ambientLight; // The ambient light parameters
uniform DirectionalLight u_directionalLight; // The directional light parameters
uniform PointLight[10] u_pointLights; // The point lights parameters
uniform SpotLight u_spotLight; // The spot light parameters
uniform sampler2D u_texture; // The texture

in vec3 v_position; // The fragment position in the world space
in vec3 v_normal; // The fragment normal for lighting
in vec2 v_textureCoordinates; // The texture coordinates

out vec4 color; // The fragment color

void main() {
    // Make sure that the normal has a unit length after the interpolation
    vec3 normal = normalize(v_normal);

    //Calculate the light and specular colors
    vec3 lightColor = u_ambientLight.color;

    lightColor += calculateDirectionalLightColor(u_directionalLight, normal);

    vec3 specularColor = vec3(0.0);

    for (int i = 0; i < 10; i++) {
        PointLight pointLight = u_pointLights[i];

        if (pointLight.enabled) {
            Colors colors = calculatePointLightColor(pointLight, u_cameraPosition, normal, v_position, u_specular, u_shininess);
            lightColor += colors.light;
            specularColor += colors.specular;
        }
    }

    if (u_spotLight.enabled) {
        Colors colors = calculateSpotLightColor(u_spotLight, u_cameraPosition, normal, v_position, u_specular, u_shininess);
        lightColor += colors.light;
        specularColor += colors.specular;
    }

    // Set the fragment color based on the texture, light and specular colors
    color = texture(u_texture, v_textureCoordinates) * vec4(lightColor, 1.0) + vec4(specularColor, 0.0);
}
