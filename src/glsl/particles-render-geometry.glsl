#version 400 core

const int PARTICLE_TYPE_NORMAL = 1;

layout (points) in; // The geometry shader input type
layout (triangle_strip, max_vertices = 4) out; // The geometry shader output type and the maximum number of vertices that can be output

uniform mat4 u_matrix; // The transformation matrix

uniform vec3 u_right; // The camera right dircetion vector
uniform vec3 u_up; // The camera up direction vector

// The variables that are passed from the vertex shader
in vec3 v_color[];
in float v_lifetime[];
in float v_size[];
in int v_type[];

out vec2 v_textureCoordinates; // The texture coordinates
out vec4 v_colorWithAlpha; // The particle color with the alpha based on the lifetime

void main() {
    // The generator particle is not rendered
    if (v_type[0] == PARTICLE_TYPE_NORMAL) {
        vec3 particlePosition = gl_in[0].gl_Position.xyz;
        
        float size = v_size[0];

        // Calculate the particle alpha so that it dissapears during the last second of the lifetime
        v_colorWithAlpha = vec4(v_color[0], min(v_lifetime[0], 1.0));

        // Calculate the positions of the square vertices so that the particle is always parallel to the near clip plane
        vec3 vertexPosition = particlePosition + (-u_right - u_up) * size;
        v_textureCoordinates = vec2(0.0, 0.0);
        gl_Position = u_matrix * vec4(vertexPosition, 1.0);
        EmitVertex();

        vertexPosition = particlePosition + (-u_right + u_up) * size;
        v_textureCoordinates = vec2(0.0, 1.0);
        gl_Position = u_matrix * vec4(vertexPosition, 1.0);
        EmitVertex();

        vertexPosition = particlePosition + (u_right - u_up) * size;
        v_textureCoordinates = vec2(1.0, 0.0);
        gl_Position = u_matrix * vec4(vertexPosition, 1.0);
        EmitVertex();

        vertexPosition = particlePosition + (u_right + u_up) * size;
        v_textureCoordinates = vec2(1.0, 1.0);
        gl_Position = u_matrix * vec4(vertexPosition, 1.0);
        EmitVertex();

        EndPrimitive();
    }
}
